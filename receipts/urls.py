from django.urls import path
from receipts.views import (
    list_receipt,
    create_receipt,
    category_list,
    account_list,
    create_expense,
    create_account
)

urlpatterns = [
    path("", list_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_expense, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
