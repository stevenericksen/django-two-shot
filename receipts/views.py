from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def list_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list_receipt": receipt_list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': categories,
    }
    return render(request, 'receipts/categories.html', context)


@login_required
def account_list(request):
    accts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        'form': form,
    }
    return render(request, "categories/create_expense.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            acct = form.save(commit=False)
            acct.owner = request.user
            acct.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        'form': form,
    }
    return render(request, "accounts/create_account.html", context)
